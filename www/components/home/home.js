(function() {
  'use strict';

  angular.module('App')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['NFCFactory'];

  function HomeController(NFCFactory) {
    var vm = this;

    vm.tag = NFCFactory.tag;
    vm.clear = function() {
      NFCFactory.clearTag();
    };
  }
})();
