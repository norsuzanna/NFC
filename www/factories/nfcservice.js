(function() {
  'use strict';

  angular.module('App')
    .factory('NFCFactory', NFCFactory);

  NFCFactory.$inject = ['$rootScope', '$ionicPlatform'];

  function NFCFactory($rootScope, $ionicPlatform) {
    var tag = {};

    $ionicPlatform.ready(function() {
      nfc.addNdefListener(function(nfcEvent) {
        console.log(JSON.stringify(nfcEvent.tag, null, 4));
        $rootScope.$apply(function() {
          angular.copy(nfcEvent.tag, tag);
          // if necessary $state.go('some-route')
        });
      }, function() {
        console.log("Listening for NDEF Tags.");
      }, function(reason) {
        alert("Error adding NFC Listener " + reason);
      });

    });

    return {
      tag: tag,

      clearTag: function() {
        angular.copy({}, tag);
      }
    };
  }
})();
